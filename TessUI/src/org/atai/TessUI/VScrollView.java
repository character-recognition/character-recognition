/*
 *
 *  Copyright 2014 Li-Cheng (Andy) Tai
 *                 atai@atai.org
 *                 All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
package org.atai.TessUI;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ScrollView;

import java.util.Vector;

public class VScrollView extends ScrollView {

    Vector<View> mTouchHandlingViews = new Vector<View>();

    public VScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public void addToTouchHandlingViews(View view) {
        mTouchHandlingViews.add(view);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        float x = ev.getRawX();
        float y = ev.getRawY();
        int i;
        int n = mTouchHandlingViews.size();
        for (i = 0; i < n; i++) {
            View v = mTouchHandlingViews.get(i);
            if (isPointInsideView(x, y, v))
                    return false;
        }

        return super.onInterceptTouchEvent(ev);
    }

    /* from http://stackoverflow.com/questions/4165765/how-to-get-a-view-from-an-event-coordinates-in-android */
    /**
     * Determines if given points are inside view
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    private boolean isPointInsideView(float x, float y, View view) {
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];

        //point is inside view bounds
        if(( x > viewX && x < (viewX + view.getWidth())) &&
                ( y > viewY && y < (viewY + view.getHeight()))) {
            return true;
        } else {
            return false;
        }
    }
}